const fs = require("fs");

const writeToFile = () => {
  const fileData = process.argv.slice(3).join(" ");

  fs.writeFile(process.argv[2], fileData, { flag: "a+" }, (err) => {
    if (err) throw new Error("Write file error");
    console.log("File was created or updated");
  });
};

writeToFile();
